from flask import Flask, render_template, request, jsonify

import speech_recognition as sr

app = Flask(__name__)
myset = {"muz", "elma", "armut"}
keywordrecord = []


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')
    if request.method == 'POST':
        words = request.form.get('key_words')
        keywords = words.split(' ')
        for word in keywords:
            myset.add(word)
        print(myset)
        return render_template('index.html')


@app.route('/speech_to_text')
def text_to_speech():
    return render_template('purejs.html')


@app.route('/keywordclear')
def clearlist():
    keywordrecord.clear()
    return ""


@app.route('/audio2', methods=['POST'])
def audio2():
    with open('tmp/audio.wav', 'wb') as f:
        f.write(request.data)
    data = process_audio()
    if data is not None:
        keywordrecord.append(data)
    print(keywordrecord)
    res = ""
    res = ",".join(keywordrecord)
    print(res)
    return jsonify(res)


def get_from_file():
    pass


def process_audio():
    r = sr.Recognizer()

    with sr.AudioFile('tmp/audio.wav') as source:
        try:
            return_text = ""
            mylistelisten = []
            audio = r.record(source)
            message = (r.recognize_google(audio, language='tr', show_all=True))
            print(message)
            if not message:
                pass
            else:
                for num, texts in enumerate(message['alternative']):
                    return_text = texts['transcript']
                    return_text = return_text.lower()
                    mylistelisten += return_text.split()
                    print(return_text)

                inter = myset.intersection(mylistelisten)
                print(inter)
                isEmpty = (len(inter) != 0)

                if isEmpty:
                    mystr = ",".join(inter)
                    return mystr


        except sr.UnknownValueError:
            print('Speech Recognition could not Understand audio')
        except sr.RequestError as e:
            print('Could not request result from Speech Recogniser Service')


@app.route('/audio', methods=['POST'])
def audio():
    with open('tmp/audio.wav', 'wb') as f:
        f.write(request.data)

    text = process_audio()

    return text


if __name__ == "__main__":
    app.run(debug=True)
